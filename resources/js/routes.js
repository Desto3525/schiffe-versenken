import Welcome          from './components/Welcome'
import Documentation    from './components/Documentation'
import Play             from './components/Play'

export const routes = [
    {
        path: '/',
        component: Welcome,
        name: 'Welcome' },
    {
        path: '/play',
        component: Play,
        name: 'Play' },
    {
        path: '/documentation',
        component: Documentation,
        name: 'Documentation' },
];