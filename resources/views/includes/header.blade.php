<header>
    <v-toolbar color="primary" dark>

        <v-toolbar-items>
            <v-btn href="{{ url('/') }}" icon large>
                <v-avatar tile>
                    <img src="images/logo.png">
                </v-avatar>
            </v-btn>
            <v-divider class="mx-3" inset vertical></v-divider>
            <v-btn href="{{ url('/') }}" flat>Battle Ship</v-btn>
        </v-toolbar-items>

        <v-spacer></v-spacer>

        <v-toolbar-items class="hidden-sm-and-down">
            <v-divider vertical></v-divider>
            <v-btn href="{{ url('/') }}" flat>Startseite</v-btn>
            <v-divider vertical></v-divider>
            <v-btn href="{{ url('/dokumentation') }}" flat>Dokumentation</v-btn>
            <v-divider vertical></v-divider>
        </v-toolbar-items>

        @auth
            <v-btn href="{{ url('/play') }}" flat>Play</v-btn>
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @else
            <div style="padding-left: 16px;">

                <v-btn color="grey" dark large>Anmelden</v-btn>
                <v-layout row justify-center>
                    <v-dialog persistent max-width="600px">
                        <v-card>
                            <v-card-title class="headline">test</v-card-title>
                        </v-card>
                    </v-dialog>
                </v-layout>

            @if (Route::has('register'))
                <v-btn href="{{ route('register') }}" color="green" dark large>Registrieren</v-btn>
            @endif
            </div>
        @endauth

    </v-toolbar>
</header>