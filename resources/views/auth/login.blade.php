@extends('layouts.app')

@section('content')

    <v-hover>
        <v-card slot-scope="{ hover }" :class="`elevation-${hover ? 12 : 2}`" class="mx-auto" width="600" style="margin-top: 100px;">
            <v-flex lg class="grey lighten-4">
                <v-container class="text-xs-center">

                    <v-card-title primary-title>
                        <h4>Anmelden</h4>
                    </v-card-title>
                    <v-form method="POST" action="{{ route('login') }}">
                        @csrf
                        <v-text-field prepend-icon="person" id="email" label="Email-Adresse" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder=" " name="email" required autofocus></v-text-field>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        <v-text-field prepend-icon="lock" id="password" type="password" label="Passwort" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder=" " name="password" required></v-text-field>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <v-layout align-center justify-center row fill-height/>
                            <v-btn type="submit" style="max-width: 300px" color="primary" large block>{{ __('Login') }}</v-btn>
                        </v-layout>
                    </v-form>

                </v-container>
            </v-flex>
        </v-card>
    </v-hover>

@endsection
