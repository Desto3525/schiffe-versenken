<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body>

        <div id="app">
            <app></app>
        </div>

        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>